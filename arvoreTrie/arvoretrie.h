/*

== Trabalho de ED-II
====

Arvore TRIE
By Andre Guimaraes Peil

*/

#ifndef ARVORETRIE_H_
#define ARVORETRIE_H_

struct no {

	struct no * pai;
	struct no * filho[27];	
	
};

typedef struct no nodo;

nodo * init();
char incluir(char * palavra, nodo * raiz);
char buscar(char * palavra, nodo * raiz);
char remover(char * palavra, nodo * raiz);
int validaNodo(nodo * ultimo);
int lenght(char * palavra);

#endif
