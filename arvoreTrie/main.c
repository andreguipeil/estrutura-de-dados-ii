/*

== Trabalho de ED-II
====

Arvore TRIE
By Andre Guimaraes Peil

*/

#include <stdio.h>
#include <stdlib.h>
#include "arvoretrie.h"
#define TAM 10001


int main()
{

	char palavra[TAM];
	char opt;
	nodo * trie;
	trie = init();
	
		// faz a entrada para dois atributos 
		// tipo de acao
		// palavra	
		while(scanf("%c %s", &opt, palavra) == 2) {
			
			switch (opt){
				
				case 'i': 
					printf("%c\n", incluir(palavra, trie));
					getchar();
					break;
				
				case 'b':	
					printf("%c\n", buscar(palavra, trie));
					getchar();
					break;
				
				case 'r':
					printf("%c\n", remover(palavra, trie));
					getchar();	
					break;
				
				case '@':
					printf("@\n");
					getchar();
					exit(1);
					break;
			}
		}
	
	return 0;	
}
