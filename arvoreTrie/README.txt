

=====================================================

  Trabalho 1 - Estrutura de Dados II
 
	Tarefa: implementação de uma árvore trie
 
  --
    Feito por: André Guimarães Peil - 10206132

======================================================




   Tutorial
## 	

	Passo 1
	-------------------
	- abra o terminal
	- acesse a pasta contendo os arquivos do programa

	Passo 2
	-------------------
	
	- ainda no terminal execute o arquivo makefile
	  $ make

	Passo 3
	-------------------
	- digite os seguintes comandos para execução do programa:
	  $ ./exe < entrada.txt > saida.txt
	- este comando gera a saida do programa, com o arquivo entrada.txt como parametro.
	
	Passo 4
	-------------------
	- para a comparação dos arquivos pode se usar tanto 
	  $ diff saida.txt saida_desejada.txt
	- ou
	  $ cmp saida.txt saida_desejada.txt
	

Observações:
##
	* Pode haver erro no final do programa, este algoritmo espera 2 argumentos para sair corretamente.

	Exemplo:
		 A linha 10001 do arquivo de entrada deve conter: @ sair
			-- onde @ sinaliza a saida.
			-- e o argumento 'sair' somente para complementar os 2 argumentos.


###################################################################################################


   Relatório
##
  
  - Há a possibilidade de acusar um erro dependendo do gerador no final do arquivo na instrução @.
	--> uma saida perfeita é @ + palavra EX: @ sair

==============

	
	PALAVRAS  #  LETRAS  #  RESULTADO
	 10000    #    1     #    OK
	 10000	  #  144     #    OK
	 10000	  #  1000    #    Ok
	 10000	  #  6000    #    OK
	 10000	  #  8000    #   MORTO
	 10000	  #  15000   #   MORTO
	 10000	  #  30000   #   FALHA DE SEGMENTAÇÃO




   Com palavras inteiras 10000 por 3000, 3500, 6000

	PALAVRAS  #  LETRAS  #  RESULTADO
	 10000    #  3000    #    OK
	 10000	  #  3500    #    MORTO  -> travada básica no computador, musica parou por uns 2 segundos firebug travou. (leu até a linha 8192)
	 10000	  #  6000    #    MORTO  -> travada básica no computador, musica parou por uns 2 segundos. (leu até a linha 4096)
	 


















