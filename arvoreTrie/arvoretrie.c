/*

== Trabalho de ED-II
====

Arvore TRIE
By Andre Guimaraes Peil

*/

#include <stdio.h>
#include <stdlib.h>
#include "arvoretrie.h"

// inicializa os valores da trie
nodo * init() {

 nodo * trie;
 int i;

	if (!(trie = malloc(sizeof(nodo)))){
		printf("\n");
		printf("Erro no malloc da arvore.");
		printf("\n");
		exit(1);
	}

	trie->pai = NULL;

	for (i = 0; i < 27; i++){
		trie->filho[i] = NULL;
	}

	return trie;
}

char incluir (char * palavra, nodo * raiz) {
	
	int tam, i, letra;
	nodo * aux; // cria um nodo para manipular a raiz
	
	tam = lenght(palavra);
	aux = raiz;

		for (i = 0; i < tam; i++) {
			letra = palavra[i]-'a';
			
			
			if(aux->filho[letra]) { 			// se aux->filho[letra] existir,  
												// significa que ela ja foi adicionada,
												// entao entra na proxima letra
				aux = aux->filho[letra];
				
			} else {							// se não tem a letra na arvore
				aux->filho[letra] = init();		// cria um novo nodo e liga dizendo que é o filho
				aux = aux->filho[letra];
			}
			
		}
	
		if (!(aux->filho[26])){					// se ainda não existir essa palavra cria ela
			aux->filho[26] = init();			// sinaliza o caracter especial criando a palavra se ainda não existir
		}
	
	return 'v';
	
}

char buscar (char * palavra, nodo * raiz) {
	nodo  * aux;
	int letra, i, tam;

	tam = lenght(palavra);
	aux = raiz;
	
	for (i = 0; i < tam; i++){
	
		letra = palavra[i] - 'a';			// descubro a letra
		aux = aux->filho[letra];   			// aux vai para o aux filho
		
		if(!aux)							// verifica se o aux existe
			return 'f';						// se não existe retorna falso
											// se existir continua pesquisando ate chegar no final
	}
	if (aux->filho[26])						// quando chega no final verifica se a palavra eh considerada uma palavra,
											// como palavra(precisa ter um ultimo caracter 26).
		return 'v';							// retorna verdadeiro
	
	return 'f';
}

char remover (char * palavra, nodo * raiz) {
	nodo  * aux;
	int letra, i, tam, flag = 0;

	tam = lenght(palavra);
	aux = raiz;
	
	for (i = 0; i < tam; i++){				// percorre o tamanho
		letra = palavra[i] - 'a';			// descobre a letra
		aux = aux->filho[letra];			// avança se tem filho
		if(!aux)							// verifica se o nodo existe
			return 'f';						// retorna falso se nao existe
	}	
	
	if(!aux->filho[26])						// se nao existe o filho 26 nao existe palavra procurada
		return 'f';
	
	aux->filho[26] = NULL;					// remove o filho que sinaliza a palavra
	
	while(aux->pai != NULL){				// enquanto o pai nao for nulo
		flag = validaNodo(aux);				// flag recebe 0 ou 1 vai para valida nodo
				
		if (!flag)							// se a flag for 0 entao
			free(aux->filho);				// posso excluir o nodo
			
		aux = aux->pai;						// e passa para o proximo
	}	
		
		return 'v';							// significa que a flag é 1
}

int validaNodo(nodo * ultimo) {	
	int i, valida = 0;
	nodo * aux;
	aux = ultimo;
		
		for (i = 0; i < 26; i++){			// percorre o vetor
			if (aux->filho[i])				// verifica se o filho[i] existe ou é nulo
				valida = 1;					// se existir retorna 1
		}
		
	return valida;							// senão passa com 0
}

int lenght(char * palavra) {
	int tam;

	for (tam = 0; palavra[tam] != '\0'; tam++);

	return tam;
}


